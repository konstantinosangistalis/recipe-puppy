//
//  RecipePuppyNetworkTests.swift
//  RecipePuppy
//
//  Created by Konstantinos Angistalis on 25/09/2017.
//  Copyright © 2017 Konstantinos Angistalis. All rights reserved.
//

import XCTest

import Moya

@testable import RecipePuppy


// Helper Method
fileprivate func url(_ route: TargetType) -> String {
    return route.baseURL.appendingPathComponent(route.path).absoluteString
}


class RecipePuppyNetworkFailureTests: XCTestCase {
    
    var networkProvider: RecipePuppyProvider? = nil
    var stubProvider: MoyaProvider<RecipePuppyAPI>? = nil
    
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        let endpointsClosure: MoyaProvider<RecipePuppyAPI>.EndpointClosure = { (target: RecipePuppyAPI) -> Endpoint<RecipePuppyAPI> in
            
            let sampleResponseClosure = { () -> EndpointSampleResponse in
                return .networkResponse(500, Data())  // you can & should have this configurable - globals, or properties on your Target
            }
            
            // all the request properties should be the same as your default provider
            let method = target.method
            let parameters = target.parameters
            let endpoint = Endpoint<RecipePuppyAPI>(url: url(target), sampleResponseClosure: sampleResponseClosure, method: method, parameters: parameters)
            return endpoint
        }
        
        stubProvider = MoyaProvider<RecipePuppyAPI>(endpointClosure: endpointsClosure, stubClosure: MoyaProvider.immediatelyStub)
        
        networkProvider = RecipePuppyProvider(with: stubProvider!)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        
        stubProvider = nil
        networkProvider = nil
    }
    
    func testPlayersAPIFailure() {
        
        //We test here the network stack and see if a failure is coming from a failing stabbed response
        
        // Wait for the response closure to be invoked
        let responseExpectation = expectation(description: "RecipePuppyProvider is fetching the stubbed response")
        
        _ = networkProvider?.search(recipe: "test", page: 1) { (response) in
            
            switch response {
                
            case .success(_):
                XCTAssert(false, "RecipePuppyProvider should not return success with server error response")
                
            case .error(_):
                break
            }
            
            responseExpectation.fulfill()
        }
        
        // Wait for the expectation to be fulfilled
        waitForExpectations(timeout: 1) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
}


class RecipePuppyNetworkSuccessTests: XCTestCase {
    
    var networkProvider: RecipePuppyProvider? = nil
    var stubProvider: MoyaProvider<RecipePuppyAPI>? = nil
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        let endpointsClosure: MoyaProvider<RecipePuppyAPI>.EndpointClosure = { (target: RecipePuppyAPI) -> Endpoint<RecipePuppyAPI> in
            
            let sampleResponseClosure = { () -> EndpointSampleResponse in
                
                // Load the sample data from a local JSON file
                let filePath = Bundle(for: type(of: self)).path(forResource: "SearchData", ofType: "json")
                let sampleData = FileManager.default.contents(atPath: filePath!)!
                
                return .networkResponse(200, sampleData)
            }
            
            // all the request properties should be the same as your default provider
            let method = target.method
            let parameters = target.parameters
            let endpoint = Endpoint<RecipePuppyAPI>(url: url(target), sampleResponseClosure: sampleResponseClosure, method: method, parameters: parameters)
            return endpoint
        }
        
        stubProvider = MoyaProvider<RecipePuppyAPI>(endpointClosure: endpointsClosure, stubClosure: MoyaProvider.immediatelyStub)
        
        networkProvider = RecipePuppyProvider(with: stubProvider!)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        
        stubProvider = nil
        networkProvider = nil
    }
    
    func testPlayersAPISuccess() {
        
        //We test here the network stack and see if a success is coming from a proper stabbed response
        
        // Wait for the response closure to be invoked
        let responseExpectation = expectation(description: "RecipePuppyProvider is fetching the stubbed response")
        
        _ = networkProvider?.search(recipe: "test", page: 1) { (response) in
            
            switch response {
                
            case .success(let wrapper):
                XCTAssertNotNil(wrapper, "RecipePuppyProvider should not return no object with successfull stubbed response")
                
            case .error(_):
                XCTAssert(false, "RecipePuppyProvider should not return error with successfull stubbed response")
            }
            
            responseExpectation.fulfill()
        }
        
        // Wait for the expectation to be fulfilled
        waitForExpectations(timeout: 1) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
}
