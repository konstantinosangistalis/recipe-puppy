//
//  RecipePuppyModelTests.swift
//  RecipePuppyModelTests
//
//  Created by Konstantinos Angistalis on 25/09/2017.
//  Copyright © 2017 Konstantinos Angistalis. All rights reserved.
//

import XCTest

import Unbox


@testable import RecipePuppy

class RecipePuppyModelTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testRecipeParsing() {
        
        let sampleResponse = ["title": "Vegetable-Pasta Oven Omelet",
                              "href": "http://find.myrecipes.com/recipes/recipefinder.dyn?action=displayRecipe&recipe_id=520763",
                              "ingredients": "tomato, onions, red pepper, garlic, olive oil, zucchini, cream cheese, vermicelli, eggs, parmesan cheese, milk, italian seasoning, salt, black pepper",
                              "thumbnail": "http://img.recipepuppy.com/560556.jpg"]
        
        let recipe = try? Recipe(unboxer: Unboxer(dictionary: sampleResponse))
        XCTAssertNotNil(recipe, "Failed to parse sample Recipe")
    }
    
    func testRecipeResponseWrapperParsing() {
        
        let sampleResponse = ["title": "Recipe Puppy",
                              "version": 0.1,
                              "href": "http://www.recipepuppy.com/",
                              "results": [["title": "Vegetable-Pasta Oven Omelet",
                                           "href": "http://find.myrecipes.com/recipes/recipefinder.dyn?action=displayRecipe&recipe_id=520763",
                                           "ingredients": "tomato, onions, red pepper, garlic, olive oil, zucchini, cream cheese, vermicelli, eggs, parmesan cheese, milk, italian seasoning, salt, black pepper",
                                           "thumbnail": "http://img.recipepuppy.com/560556.jpg"]]] as [String : Any]
        
        let response = try? ResponseWrapper<Recipe>(unboxer: Unboxer(dictionary: sampleResponse))
        XCTAssertNotNil(response, "Failed to parse sample ResponseWrapper<Recipe>")
        XCTAssertNotNil(response?.results.first, "Failed to parse sample Recipe")
    }
}
