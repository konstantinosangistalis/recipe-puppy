//
//  RecipePuppySearchViewModelTests.swift
//  RecipePuppy
//
//  Created by Konstantinos Angistalis on 26/09/2017.
//  Copyright © 2017 Konstantinos Angistalis. All rights reserved.
//

import XCTest

import Moya

@testable import RecipePuppy


// Helper Method
fileprivate func url(_ route: TargetType) -> String {
    return route.baseURL.appendingPathComponent(route.path).absoluteString
}


class RecipePuppySearchViewModelTests: XCTestCase {
    
    var networkProvider: RecipePuppyProvider? = nil
    var stubProvider: MoyaProvider<RecipePuppyAPI>? = nil
    var viewModel: SearchViewModel!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        let endpointsClosure: MoyaProvider<RecipePuppyAPI>.EndpointClosure = { (target: RecipePuppyAPI) -> Endpoint<RecipePuppyAPI> in
            
            var sampleResponseClosure: () -> EndpointSampleResponse
            
            // TODO: Each test should have it's own sample response file
            if let page = target.parameters?["p"] as? Int, page == 2 {
                
                sampleResponseClosure = { () -> EndpointSampleResponse in
                    
                    // Load the sample data from a local JSON file
                    let filePath = Bundle(for: type(of: self)).path(forResource: "SearchDataPage2", ofType: "json")
                    let sampleData = FileManager.default.contents(atPath: filePath!)!
                    
                    return .networkResponse(200, sampleData)
                }
                
            } else if let page = target.parameters?["p"] as? Int, page == 3 {
                
                sampleResponseClosure = { () -> EndpointSampleResponse in
                    
                    // Load the sample data from a local JSON file
                    let filePath = Bundle(for: type(of: self)).path(forResource: "SearchDataPage3", ofType: "json")
                    let sampleData = FileManager.default.contents(atPath: filePath!)!
                    
                    return .networkResponse(200, sampleData)
                }
                
            } else {
                sampleResponseClosure = { () -> EndpointSampleResponse in
                    
                    // Load the sample data from a local JSON file
                    let filePath = Bundle(for: type(of: self)).path(forResource: "SearchData", ofType: "json")
                    let sampleData = FileManager.default.contents(atPath: filePath!)!
                    
                    return .networkResponse(200, sampleData)
                }
            }
            
            // all the request properties should be the same as your default provider
            let method = target.method
            let parameters = target.parameters
            let endpoint = Endpoint<RecipePuppyAPI>(url: url(target), sampleResponseClosure: sampleResponseClosure, method: method, parameters: parameters)
            return endpoint
        }
        
        stubProvider = MoyaProvider<RecipePuppyAPI>(endpointClosure: endpointsClosure, stubClosure: MoyaProvider.immediatelyStub)
        
        networkProvider = RecipePuppyProvider(with: stubProvider!)
        
        viewModel = SearchViewModel(networkProvider!)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        
        stubProvider = nil
        networkProvider = nil
        viewModel = nil
    }
    
    func testSearch() {
        
        // Wait for the response closure to be invoked
        let responseExpectation = expectation(description: "RecipePuppyProvider is fetching the stubbed response")
        
        viewModel.search(recipe: "testRecipe") { [unowned self] (errorMessage) in
            
            XCTAssertNil(errorMessage, "SearchViewModel should not return an error message with successfull stubbed response")
            XCTAssert(self.viewModel.numberOfRows() == 10, "SearchViewModel return wrong number of search results")
            
            self.viewModel.clearSearchResults()
            XCTAssert(self.viewModel.numberOfRows() == 0, "SearchViewModel return wrong number of search results after clear action")

            responseExpectation.fulfill()
        }
        
        // Wait for the expectation to be fulfilled
        waitForExpectations(timeout: 1) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    
    func testSearchTableViewDecorating() {
        
        // Wait for the response closure to be invoked
        let responseExpectation = expectation(description: "RecipePuppyProvider is fetching the stubbed response")
        
        viewModel.search(recipe: "testRecipe") { [unowned self] (errorMessage) in
            
            let indexPath = IndexPath(row: 2, section: 0)
            
            // TODO: Load these directly from the file
            XCTAssert(self.viewModel.title(at: indexPath) == "Ramen Noodle Egg Foo Yung", "SearchViewModel returns wrong titles")
            XCTAssert(self.viewModel.subtitle(at: indexPath) == "baking powder, butter, eggs, green onion, ramen noodles, vegetable oil, soy sauce", "SearchViewModel returns wrong subtitle")
            XCTAssert(self.viewModel.imageURL(at: indexPath)?.absoluteString == "http://img.recipepuppy.com/34856.jpg", "SearchViewModel returns wrong thumbnail URL")

            responseExpectation.fulfill()
        }
        
        // Wait for the expectation to be fulfilled
        waitForExpectations(timeout: 1) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    
    func testSearchPagination() {
        
        // Wait for the response closure to be invoked
        let responseExpectation = expectation(description: "RecipePuppyProvider is fetching the stubbed 1st page")
        
        viewModel.search(recipe: "testRecipe") { [unowned self] (errorMessage) in
            
            XCTAssertNil(errorMessage, "SearchViewModel should not return an error message with successfull stubbed response")
            XCTAssert(self.viewModel.numberOfRows() == 10, "SearchViewModel returns wrong number of search results")
            XCTAssert(self.viewModel.canLoadMorePages(), "SearchViewModel returns wrong paintation indicator")
                        
            self.viewModel.loadMore { [unowned self] (_) in
                
                XCTAssert(self.viewModel.numberOfRows() == 20, "SearchViewModel returns wrong number of pagination results")
                XCTAssert(self.viewModel.canLoadMorePages(), "SearchViewModel returns wrong paintation indicator")
                
                self.viewModel.loadMore { [unowned self] (_) in
                    
                    XCTAssert(self.viewModel.numberOfRows() == 22, "SearchViewModel returns wrong number of pagination results")
                    XCTAssert(self.viewModel.canLoadMorePages() == false, "SearchViewModel returns wrong paintation indicator")
                    
                    self.viewModel.search(recipe: "testRecipe") { [unowned self] (errorMessage) in
                        
                        XCTAssert(self.viewModel.numberOfRows() == 10, "SearchViewModel returns wrong number of search results")
                        XCTAssert(self.viewModel.canLoadMorePages(), "SearchViewModel returns wrong paintation indicator")
                        
                        responseExpectation.fulfill()
                    }
                }
            }
        }
        
        // Wait for the expectation to be fulfilled
        waitForExpectations(timeout: 1) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
}
