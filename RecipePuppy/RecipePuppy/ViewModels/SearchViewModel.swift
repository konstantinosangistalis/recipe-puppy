//
//  SearchViewModel.swift
//  RecipePuppy
//
//  Created by Konstantinos Angistalis on 26/09/2017.
//  Copyright © 2017 Konstantinos Angistalis. All rights reserved.
//

import Foundation

import Moya


class SearchViewModel {
    
    /// The result of the search request
    fileprivate let networkProvider: RecipePuppyProvider
    fileprivate var searchResult: ResponseWrapper<Recipe>?
    fileprivate var request: Cancellable?

    fileprivate var currentQuery: String?
    fileprivate var currentPage = 1
    
    fileprivate var isEndOfResults = false
    fileprivate var paginationRequest: Cancellable?

    
    /// Init with the shared provider as default
    required init(_ provider: RecipePuppyProvider = RecipePuppyProvider.shared) {
        self.networkProvider = provider
    }
    
    deinit {
        request?.cancel()
    }
}


// MARK: - Data Methods

extension SearchViewModel {
    
    /// Search for a recipe
    func search(recipe query: String, completion: @escaping (String?) -> Void) {
        
        // Cancel the previous queries
        request?.cancel()
        
        // Rest the previous pagination state
        resetPagination()
        
        request = networkProvider.search(recipe: query, page: currentPage) { [weak self] (response) in
            
            guard let blockSelf = self else {
                return
            }
            
            switch response {
                
            case .success(let wrapper):
                // Save the result
                blockSelf.currentQuery = query
                blockSelf.searchResult = wrapper
                
                if wrapper.results.count < 10 {
                    blockSelf.isEndOfResults = true
                    debugPrint("Reached end of results")
                }
                
                completion(nil)
                
            case .error(let localisedMessage):
                // Clear the last results and pass the error message
                blockSelf.searchResult = nil
                completion(localisedMessage)
            }
            
            blockSelf.request = nil
        }
    }
    
    func clearSearchResults() {
        
        // Cancel the previous query
        request?.cancel()
        request = nil
        searchResult = nil
    
        resetPagination()
    }
}


// MARK: - TableViewDecorating Methods

extension SearchViewModel: TableViewDecorating {
    
    func numberOfRows() -> Int {
        return searchResult?.results.count ?? 0
    }
    
    func title(at indexPath: IndexPath) -> String? {
        
        // Make sure we are not of of bounds of the array
        guard let recipes = searchResult?.results, indexPath.row < recipes.count else {
            return nil
        }
        
        let result: String?
        
        do {
            result = try recipes[indexPath.row].title.convertHtmlSymbols()
        } catch {
            result = recipes[indexPath.row].title
        }
        
        return result
    }
    
    func subtitle(at indexPath: IndexPath) -> String? {
        
        // Make sure we are not of of bounds of the array
        guard let recipes = searchResult?.results, indexPath.row < recipes.count else {
            return nil
        }
        
        return recipes[indexPath.row].ingredients
    }
    
    func imageURL(at indexPath: IndexPath) -> URL? {
        
        // Make sure we are not of of bounds of the array
        guard let recipes = searchResult?.results, indexPath.row < recipes.count else {
            return nil
        }
        
        return URL(string: recipes[indexPath.row].thumbnail)
    }
    
    func detailsURL(at indexPath: IndexPath) -> URL? {
        
        // Make sure we are not of of bounds of the array
        guard let recipes = searchResult?.results, indexPath.row < recipes.count else {
            return nil
        }
        
        return URL(string: recipes[indexPath.row].href)
    }
}


// MARK: - Pagiantion

extension SearchViewModel: Paginating {
    
    fileprivate func resetPagination() {
        paginationRequest?.cancel()
        paginationRequest = nil
        isEndOfResults = false
        currentPage = 1
    }
    
    func canLoadMorePages() -> Bool {
        return !isEndOfResults
    }
    
    func loadMore(_ completion: @escaping (String?) -> Void) {
        
        // Wait for the last pagination request to finish
        guard isEndOfResults == false, paginationRequest == nil, let currentQuery = currentQuery else {
            return
        }
        
        debugPrint("Fetcing page \(currentPage + 1)")
        
        paginationRequest = networkProvider.search(recipe: currentQuery, page: currentPage + 1) { [weak self] (response) in
            
            guard let blockSelf = self else {
                return
            }
            
            blockSelf.paginationRequest = nil
            
            switch response {
                
            case .success(let wrapper):
                
                // Save the next page
                if wrapper.results.count > 0 {
                    blockSelf.searchResult?.results.append(contentsOf: wrapper.results)
                    
                    // Check if we have more results to fetch. Page size is 10
                    if wrapper.results.count < 10 {
                        blockSelf.isEndOfResults = true
                        debugPrint("Reached end of results")
                    } else {
                        blockSelf.currentPage += 1
                    }
                    
                } else {
                    // Mark the end of pagination
                    blockSelf.isEndOfResults = true
                    debugPrint("Reached end of results")
                }
                
                completion(nil)
                
            case .error(let localisedMessage):
                // Pass the error message
                completion(localisedMessage)
            }
        }
    }
}
