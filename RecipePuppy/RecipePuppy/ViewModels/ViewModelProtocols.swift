//
//  ViewModelProtocols.swift
//  RecipePuppy
//
//  Created by Konstantinos Angistalis on 26/09/2017.
//  Copyright © 2017 Konstantinos Angistalis. All rights reserved.
//

import Foundation


protocol TableViewDecorating {
    func numberOfRows() -> Int
    func title(at indexPath: IndexPath) -> String?
    func subtitle(at indexPath: IndexPath) -> String?
    func imageURL(at indexPath: IndexPath) -> URL?
}

protocol Paginating {
    func canLoadMorePages() -> Bool
    func loadMore(_ completion: @escaping (String?) -> Void)
}
