//
//  String+Decoding.swift
//  RecipePuppy
//
//  Created by Konstantinos Angistalis on 27/09/2017.
//  Copyright © 2017 Konstantinos Angistalis. All rights reserved.
//

import UIKit


public extension String {
    
    func convertHtmlSymbols() throws -> String? {
        guard let data = data(using: .utf8) else { return nil }
        return try NSAttributedString(data: data, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue], documentAttributes: nil).string
    }
}
