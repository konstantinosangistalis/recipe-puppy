//
//  UIViewController+Alerts.swift
//  RecipePuppy
//
//  Created by Konstantinos Angistalis on 27/09/2017.
//  Copyright © 2017 Konstantinos Angistalis. All rights reserved.
//

import UIKit

extension UIViewController {
    
    /// Show a message is an UIAlertView
    func showGenericAlert(with message: String) {
        let alertController = UIAlertController(title: "Generic Error", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
}
