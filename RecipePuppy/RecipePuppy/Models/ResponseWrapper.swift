//
//  ResponseWrapper.swift
//  RecipePuppy
//
//  Created by Konstantinos Angistalis on 25/09/2017.
//  Copyright © 2017 Konstantinos Angistalis. All rights reserved.
//

import Foundation

import Unbox


/// A single recipe
class ResponseWrapper<T: Unboxable>: Unboxable {
    
    let title: String
    let version: Float
    let href: String
    var results: [T]
    
    required init(unboxer: Unboxer) throws {
        
        title = try unboxer.unbox(key: "title")
        version = try unboxer.unbox(key: "version")
        href = try unboxer.unbox(key: "href")
        
        results = try unboxer.unbox(key: "results")
    }
}
