//
//  Recipe.swift
//  RecipePuppy
//
//  Created by Konstantinos Angistalis on 25/09/2017.
//  Copyright © 2017 Konstantinos Angistalis. All rights reserved.
//

import Foundation

import Unbox


/// A single recipe
class Recipe: Unboxable {
    
    let title: String
    let href: String
    let ingredients: String
    let thumbnail: String
    
    required init(unboxer: Unboxer) throws {
        
        title = try unboxer.unbox(key: "title")
        href = try unboxer.unbox(key: "href")
        ingredients = try unboxer.unbox(key: "ingredients")
        thumbnail = try unboxer.unbox(key: "thumbnail")
    }
}
