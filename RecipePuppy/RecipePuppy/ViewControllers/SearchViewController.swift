//
//  SearchViewController.swift
//  RecipePuppy
//
//  Created by Konstantinos Angistalis on 25/09/2017.
//  Copyright © 2017 Konstantinos Angistalis. All rights reserved.
//

import UIKit
import SafariServices

import AlamofireImage


class SearchViewController: UIViewController {

    @IBOutlet weak var ibSearchBar: UISearchBar!
    @IBOutlet weak var ibTableView: UITableView!
    
    fileprivate let viewModel = SearchViewModel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // Remove the separators from the bottom
        ibTableView.tableFooterView = UIView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        ibSearchBar.becomeFirstResponder()
    }
}


extension SearchViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecipeCellID", for: indexPath) as! RecipeTableViewCell
        
        cell.ibTitleLabel.text = viewModel.title(at: indexPath)
        cell.ibDetailsLabel.text = viewModel.subtitle(at: indexPath)
        
        cell.ibImageView.image = nil
        if let targetURL = viewModel.imageURL(at: indexPath) {
            debugPrint(targetURL)
            cell.ibImageView.af_setImage(withURL: targetURL)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let url = viewModel.detailsURL(at: indexPath) {
            
            // Show the details in a Safari controller
            let safariController = SFSafariViewController(url: url)
            present(safariController, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if viewModel.canLoadMorePages(), indexPath.row == viewModel.numberOfRows() - 3 {
            viewModel.loadMore({ [weak self] (errorMessage) in
                
                if let errorMessage = errorMessage, errorMessage != "cancelled" {
                    
                    // Show error
                    self?.showGenericAlert(with: errorMessage)
                    
                } else {
                    self?.ibTableView.reloadData()
                }
            })
        }
    }
}


extension SearchViewController: UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        ibSearchBar.resignFirstResponder()
    }
    
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        guard searchText.isEmpty == false else {
            viewModel.clearSearchResults()
            ibTableView.reloadData()
            return
        }
        
        viewModel.search(recipe: searchText) { [weak self] (errorMessage) in
            
            if let errorMessage = errorMessage, errorMessage != "cancelled" {
                
                // Show error
                self?.showGenericAlert(with: errorMessage)
                
            } else {
                self?.ibTableView.reloadData()
            }
        }
    }
}
