//
//  NetworkManager.swift
//  RecipePuppy
//
//  Created by Konstantinos Angistalis on 25/09/2017.
//  Copyright © 2017 Konstantinos Angistalis. All rights reserved.
//

import Foundation

import os.log

import Moya
import Result
import Unbox


/// Used internally for capturing the sompletion status of a network request
fileprivate enum CompletionType {
    case success(Unboxer)
    case error(statusCode: Int, userInfo: Unboxer?)
    case fail(error: MoyaError)
}

/// Used for passing data for presentation. localisedMessage can be used to pass user facing error messages
public enum NetworkUIResponse<T> {
    case success(T)
    case error(localisedMessage: String?)
}


public class Network {
    
    static let log = OSLog(subsystem: "com.angistalis.interviews.RecipePuppy", category: "Network")
    
    fileprivate static let responseQueue = DispatchQueue(label: "com.angistalis.interviews.RecipePuppy.network.responses", attributes: DispatchQueue.Attributes.concurrent)
    
    fileprivate static func process(response result: Result<Moya.Response, MoyaError>,
                                    completion completionClosure: ((CompletionType) -> Void)? = nil) {
        switch result {
            
        case let .success(response):
            do {
                // Validate the status code. Special handling of certain status codes can be done here i.e. authentication errors
                _ = try response.filterSuccessfulStatusCodes()
                
                // Create an unboxer and pass the data
                let json = try Unboxer(data: response.data)
                
                DispatchQueue.main.async {
                    completionClosure?(.success(json))
                }
            }
            catch {
                // show an error to your user
                os_log("%@", log: Network.log, type: .debug, error.localizedDescription)
                
                // We can use the unboxer here to parse any application errors from the server
                let json = try? Unboxer(data: response.data)
                
                DispatchQueue.main.async {
                    completionClosure?(.error(statusCode: response.statusCode, userInfo: json))
                }
            }
            
        case let .failure(error):
            
            // Log the error
            switch error {
            case .underlying(let underlyingError as NSError):
                
                // Do not log canceled errors
                if underlyingError.code == -999 {
                    break
                } else {
                    os_log("%@", log: Network.log, type: .error, error.localizedDescription)
                }
            default:
                os_log("%@", log: Network.log, type: .error, error.localizedDescription)
            }
            
            DispatchQueue.main.async {
                completionClosure?(.fail(error: error))
            }
        }
    }
}

/// Privider for getting data from the FanDuel API
public class RecipePuppyProvider: Network {
    
    static let shared = RecipePuppyProvider()
    
    
    // MARK: Initialisers
    
    private var internalProvider = MoyaProvider<RecipePuppyAPI>()
    
    /// Convinience initialiser for testing
    convenience init(with provider: MoyaProvider<RecipePuppyAPI>) {
        self.init()
        
        internalProvider = provider
    }
    
    private func request(_ target: RecipePuppyAPI, completion closure: ((CompletionType) -> Void)? = nil ) -> Cancellable {
        
        // Channel all the request through the common handler
        return internalProvider.request(target, queue: Network.responseQueue) { result in
            Network.process(response: result, completion: closure)
        }
    }
    
    
    // MARK: API Methods
    
    /// Search for a recipe.
    func search(recipe query: String, page: Int, completion: ((NetworkUIResponse<ResponseWrapper<Recipe>>) -> Void)? = nil ) -> Cancellable {
        
        let endpoint = RecipePuppyAPI.searchRecipe(query: query, page: page)
        
        return request(endpoint) { result in
            
            switch result {
            case .success(let unboxer):
                do {
                    let result = try ResponseWrapper<Recipe>(unboxer: unboxer)
                    completion?(NetworkUIResponse.success(result))
                    
                } catch {
                    os_log("Search response parsing failed: %@", log: Network.log, type: .error, error.localizedDescription)
                    completion?(NetworkUIResponse.error(localisedMessage: nil))
                }
            case .fail(let error):
                // Network error
                completion?(NetworkUIResponse.error(localisedMessage: error.localizedDescription))
            case .error(let code, let userInfo):
                // Server error. You can handle different codes here and show alerts if appropriate
                os_log("Search network request error: %@", log: Network.log, type: .error, "\(code) - \(String(describing: userInfo?.dictionary))")
                completion?(NetworkUIResponse.error(localisedMessage: nil))
            }
        }
    }
}


