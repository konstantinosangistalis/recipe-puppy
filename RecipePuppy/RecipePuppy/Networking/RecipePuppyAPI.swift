//
//  RecipePuppyAPI.swift
//  RecipePuppy
//
//  Created by Konstantinos Angistalis on 25/09/2017.
//  Copyright © 2017 Konstantinos Angistalis. All rights reserved.
//

import Foundation

import Moya


/// Defines the available endpoints from the API
enum RecipePuppyAPI {
    case searchRecipe(query: String, page: Int)
}

extension RecipePuppyAPI: TargetType {
    
    var baseURL: URL {
        return URL(string: "http://www.recipepuppy.com/")!
    }
    
    var path: String {
        switch self {
            
        case .searchRecipe:
            return "api/"
        }
    }
    
    var method: Moya.Method {
        switch self {
        default:
            return .get
        }
    }
    
    var parameters: [String: Any]? {
        switch self {
        case .searchRecipe(let query, let page):
            return ["q": query, "p": page]
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var multipartBody: [Moya.MultipartFormData]? {
        return nil
    }
    
    var parameterEncoding: Moya.ParameterEncoding {
        switch self {
        default:
            return URLEncoding()
        }
    }
    
    var task: Task {
        return .request
    }
}
